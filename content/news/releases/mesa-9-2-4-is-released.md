---
title:    "Mesa 9.2.4 is released"
date:     2013-11-27
category: releases
tags:     []
---
[Mesa 9.2.4](https://docs.mesa3d.org/relnotes/9.2.4.html) is released. This is a bug fix
release.
