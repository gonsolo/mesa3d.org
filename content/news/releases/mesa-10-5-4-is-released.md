---
title:    "Mesa 10.5.4 is released"
date:     2015-04-24
category: releases
tags:     []
---
[Mesa 10.5.4](https://docs.mesa3d.org/relnotes/10.5.4.html) is released. This is a bug-fix
release.
