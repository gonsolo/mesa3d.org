---
title:    "Mesa 21.2.3 is released"
date:     2021-09-29
category: releases
tags:     []
---
[Mesa 21.2.3](https://docs.mesa3d.org/relnotes/21.2.3.html) is released. This is a bug fix release.
