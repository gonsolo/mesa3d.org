---
title:    "Mesa 21.3.2 is released"
date:     2021-12-17
category: releases
tags:     []
---
[Mesa 21.3.2](https://docs.mesa3d.org/relnotes/21.3.2.html) is released. This is a bug fix release.
