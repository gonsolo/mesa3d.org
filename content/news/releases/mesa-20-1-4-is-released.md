---
title:    "Mesa 20.1.4 is released"
date:     2020-07-22
category: releases
tags:     []
---
[Mesa 20.1.4](https://docs.mesa3d.org/relnotes/20.1.4.html) is released. This is a bug fix release.
