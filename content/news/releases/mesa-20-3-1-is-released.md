---
title:    "Mesa 20.3.1 is released"
date:     2020-12-16
category: releases
tags:     []
---
[Mesa 20.3.1](https://docs.mesa3d.org/relnotes/20.3.1.html) is released. This is a bug fix release.
