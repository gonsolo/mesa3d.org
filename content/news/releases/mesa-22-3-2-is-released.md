---
title:    "Mesa 22.3.2 is released"
date:     2022-12-29
category: releases
tags:     []
---
[Mesa 22.3.2](https://docs.mesa3d.org/relnotes/22.3.2.html) is released.
This is a bug fix release.
