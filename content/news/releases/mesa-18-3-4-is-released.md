---
title:    "Mesa 18.3.4 is released"
date:     2019-02-18
category: releases
tags:     []
---
[Mesa 18.3.4](https://docs.mesa3d.org/relnotes/18.3.4.html) is released. This is a bug-fix
release.
