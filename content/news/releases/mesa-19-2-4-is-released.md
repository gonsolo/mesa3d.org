---
title:    "Mesa 19.2.4 is released"
date:     2019-11-13
category: releases
tags:     []
---
[Mesa 19.2.4](https://docs.mesa3d.org/relnotes/19.2.4.html) is released. This is an emergency
bugfix release, all users of 19.2.3 are recomended to upgrade
immediately.
