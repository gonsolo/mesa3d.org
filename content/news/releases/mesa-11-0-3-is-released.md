---
title:    "Mesa 11.0.3 is released"
date:     2015-10-10
category: releases
tags:     []
---
[Mesa 11.0.3](https://docs.mesa3d.org/relnotes/11.0.3.html) is released. This is a bug-fix
release.
