---
title:    "Mesa 22.1.7 is released"
date:     2022-08-17
category: releases
tags:     []
---
[Mesa 22.1.7](https://docs.mesa3d.org/relnotes/22.1.7.html) is released.
This is a bug fix release.
