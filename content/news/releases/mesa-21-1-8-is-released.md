---
title:    "Mesa 21.1.8 is released"
date:     2021-09-08
category: releases
tags:     []
---
[Mesa 21.1.8](https://docs.mesa3d.org/relnotes/21.1.8.html) is released. This is a bug fix release.
