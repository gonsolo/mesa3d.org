---
title:    "Mesa 18.2.6 is released"
date:     2018-11-28
category: releases
tags:     []
---
[Mesa 18.2.6](https://docs.mesa3d.org/relnotes/18.2.6.html) is released. This is a bug-fix
release.
