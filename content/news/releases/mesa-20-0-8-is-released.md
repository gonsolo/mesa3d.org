---
title:    "Mesa 20.0.8 is released"
date:     2020-06-11
category: releases
tags:     []
summary:  "[Mesa 20.0.8](https://docs.mesa3d.org/relnotes/20.0.8.html) is released. This is a bug fix
release."
---
[Mesa 20.0.8](https://docs.mesa3d.org/relnotes/20.0.8.html) is released. This is a bug fix
release.

{{< alert type="info" title="Note" >}}
It is anticipated that 20.0.8 will be the final release in the
20.0 series. Users of 20.0 are encouraged to migrate to the 20.1 series
in order to obtain future fixes.
{{< /alert >}}
