---
title:    "Mesa 10.0.4 is released"
date:     2014-03-12
category: releases
tags:     []
---
[Mesa 10.0.4](https://docs.mesa3d.org/relnotes/10.0.4.html) is released. This is a bug-fix
release.
