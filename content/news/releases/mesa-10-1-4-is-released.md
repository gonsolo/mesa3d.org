---
title:    "Mesa 10.1.4 is released"
date:     2014-05-20
category: releases
tags:     []
---
[Mesa 10.1.4](https://docs.mesa3d.org/relnotes/10.1.4.html) is released. This is a bug-fix
release.
