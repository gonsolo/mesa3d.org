---
title:    "Mesa 23.3.5 is released"
date:     2024-01-31
category: releases
tags:     []
---
[Mesa 23.3.5](https://docs.mesa3d.org/relnotes/23.3.5.html) is released.
This is a bug fix release.
