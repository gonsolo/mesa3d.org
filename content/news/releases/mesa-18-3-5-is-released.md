---
title:    "Mesa 18.3.5 is released"
date:     2019-03-18
category: releases
tags:     []
---
[Mesa 18.3.5](https://docs.mesa3d.org/relnotes/18.3.5.html) is released. This is a bug-fix
release.
