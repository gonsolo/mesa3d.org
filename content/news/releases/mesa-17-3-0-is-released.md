---
title:    "Mesa 17.3.0 is released"
date:     2017-12-08
category: releases
tags:     []
---
[Mesa 17.3.0](https://docs.mesa3d.org/relnotes/17.3.0.html) is released. This is a new
development release. See the release notes for more information about
the release.
