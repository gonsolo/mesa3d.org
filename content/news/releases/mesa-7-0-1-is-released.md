---
title:    "Mesa 7.0.1 is released"
date:     2007-08-03
category: releases
tags:     []
---
[Mesa 7.0.1](https://docs.mesa3d.org/relnotes/7.0.1.html) is released. This is a bug-fix
release.
