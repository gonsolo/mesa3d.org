---
title:    "Mesa 10.3 is released"
date:     2014-09-19
category: releases
tags:     []
summary:  "[Mesa 10.3](https://docs.mesa3d.org/relnotes/10.3.html) is released. This is a new development
release. See the release notes for more information about the release."
---
[Mesa 10.3](https://docs.mesa3d.org/relnotes/10.3.html) is released. This is a new development
release. See the release notes for more information about the release.

Also, [Mesa 10.2.8](https://docs.mesa3d.org/relnotes/10.2.8.html) is released. This is a bug
fix release from the 10.2 branch.
