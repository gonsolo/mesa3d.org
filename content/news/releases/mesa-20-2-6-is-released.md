---
title:    "Mesa 20.2.6 is released"
date:     2020-12-16
category: releases
tags:     []
---
[Mesa 20.2.6](https://docs.mesa3d.org/relnotes/20.2.6.html) is released. This
is a an emergency bug fix release for 20.2.5, anyone using 20.2.5 is recomended
to upgrade immediately.
