---
title:    "Mesa 21.3.6 is released"
date:     2022-02-09
category: releases
tags:     []
---
[Mesa 21.3.6](https://docs.mesa3d.org/relnotes/21.3.6.html) is released. This is a bug fix release.
