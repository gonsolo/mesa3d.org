---
title:    "Mesa 23.2.1 is released"
date:     2023-09-28
category: releases
tags:     []
---
[Mesa 23.2.1](https://docs.mesa3d.org/relnotes/23.2.1.html) is released.
This is a bug fix release.
