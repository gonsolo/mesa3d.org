---
title:    "Mesa 23.3.3 is released"
date:     2024-01-10
category: releases
tags:     []
---
[Mesa 23.3.3](https://docs.mesa3d.org/relnotes/23.3.3.html) is released.
This is a bug fix release.
