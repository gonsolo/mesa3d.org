---
title:    "Mesa 12.0.2 is released"
date:     2016-09-02
category: releases
tags:     []
---
[Mesa 12.0.2](https://docs.mesa3d.org/relnotes/12.0.2.html) is released. This is a bug-fix
release.
