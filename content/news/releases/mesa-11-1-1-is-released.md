---
title:    "Mesa 11.1.1 is released"
date:     2016-01-13
category: releases
tags:     []
---
[Mesa 11.1.1](https://docs.mesa3d.org/relnotes/11.1.1.html) is released. This is a bug-fix
release.
