---
title:    "Mesa 10.1.1 is released"
date:     2014-04-18
category: releases
tags:     []
---
[Mesa 10.1.1](https://docs.mesa3d.org/relnotes/10.1.1.html) is released. This is a bug-fix
release.
