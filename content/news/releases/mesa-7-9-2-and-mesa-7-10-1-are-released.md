---
title:    "Mesa 7.9.2 and Mesa 7.10.1 are released"
date:     2011-03-02
category: releases
tags:     []
---
[Mesa 7.9.2](https://docs.mesa3d.org/relnotes/7.9.2.html) and [Mesa
7.10.1](https://docs.mesa3d.org/relnotes/7.10.1.html) are released. These are stable releases
containing bug fixes since the 7.9.1 and 7.10 releases.
