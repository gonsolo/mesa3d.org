---
title:    "Mesa 19.2.8 is released"
date:     2019-12-18
category: releases
tags:     []
---
[Mesa 19.2.8](https://docs.mesa3d.org/relnotes/19.2.8.html) is released. This is a bug fix
release.
