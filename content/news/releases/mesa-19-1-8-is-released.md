---
title:    "Mesa 19.1.8 is released"
date:     2019-10-21
category: releases
tags:     []
summary:  "[Mesa 19.1.8](https://docs.mesa3d.org/relnotes/19.1.8.html) is released. This is a bug-fix
release."
---
[Mesa 19.1.8](https://docs.mesa3d.org/relnotes/19.1.8.html) is released. This is a bug-fix
release.

{{< alert type="info" title="Note" >}}
It is anticipated that 19.1.8 will be the final release in the
19.1 series. Users of 19.1 are encouraged to migrate to the 19.2 series
in order to obtain future fixes.
{{< /alert >}}
