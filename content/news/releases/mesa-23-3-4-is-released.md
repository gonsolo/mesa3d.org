---
title:    "Mesa 23.3.4 is released"
date:     2024-01-24
category: releases
tags:     []
---
[Mesa 23.3.4](https://docs.mesa3d.org/relnotes/23.3.4.html) is released.
This is a bug fix release.
