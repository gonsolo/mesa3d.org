---
title:    "Mesa 19.2.7 is released"
date:     2019-12-04
category: releases
tags:     []
---
[Mesa 19.2.7](https://docs.mesa3d.org/relnotes/19.2.7.html) is released. This is a bug fix
release.
