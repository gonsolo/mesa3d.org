---
title:    "Mesa 10.3.2 is released"
date:     2014-10-24
category: releases
tags:     []
---
[Mesa 10.3.2](https://docs.mesa3d.org/relnotes/10.3.2.html) is released. This is a bug-fix
release.
