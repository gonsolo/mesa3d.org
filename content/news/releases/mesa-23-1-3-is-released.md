---
title:    "Mesa 23.1.3 is released"
date:     2023-06-22
category: releases
tags:     []
---
[Mesa 23.1.3](https://docs.mesa3d.org/relnotes/23.1.3.html) is released.
This is a bug fix release.
