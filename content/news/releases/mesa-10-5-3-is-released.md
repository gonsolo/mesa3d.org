---
title:    "Mesa 10.5.3 is released"
date:     2015-04-12
category: releases
tags:     []
---
[Mesa 10.5.3](https://docs.mesa3d.org/relnotes/10.5.3.html) is released. This is a bug-fix
release.
