---
title:    "Mesa 22.1.2 is released"
date:     2022-06-16
category: releases
tags:     []
---
[Mesa 22.1.2](https://docs.mesa3d.org/relnotes/22.1.2.html) is released. This is a bug fix release.
