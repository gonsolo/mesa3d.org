---
title:    "Mesa 6.4.1 has been released"
date:     2005-11-29
category: releases
tags:     []
---
[Mesa 6.4.1](https://docs.mesa3d.org/relnotes/6.4.1.html) has been released. This is stable,
bug-fix release.
