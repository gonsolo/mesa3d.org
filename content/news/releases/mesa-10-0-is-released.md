---
title:    "Mesa 10.0 is released"
date:     2013-11-30
category: releases
tags:     []
---
[Mesa 10.0](https://docs.mesa3d.org/relnotes/10.0.html) is released. This is a new development
release. See the release notes for more information about the release.
