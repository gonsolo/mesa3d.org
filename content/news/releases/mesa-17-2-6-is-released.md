---
title:    "Mesa 17.2.6 is released"
date:     2017-11-25
category: releases
tags:     []
---
[Mesa 17.2.6](https://docs.mesa3d.org/relnotes/17.2.6.html) is released. This is a bug-fix
release.
