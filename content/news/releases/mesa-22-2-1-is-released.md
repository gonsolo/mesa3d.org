---
title:    "Mesa 22.2.1 is released"
date:     2022-10-11
category: releases
tags:     []
---
[Mesa 22.2.1](https://docs.mesa3d.org/relnotes/22.2.1.html) is released.
This is a bug fix release.
