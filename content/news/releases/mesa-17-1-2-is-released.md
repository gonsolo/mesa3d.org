---
title:    "Mesa 17.1.2 is released"
date:     2017-06-05
category: releases
tags:     []
---
[Mesa 17.1.2](https://docs.mesa3d.org/relnotes/17.1.2.html) is released. This is a bug-fix
release.
