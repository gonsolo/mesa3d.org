---
title:    "Mesa 22.2.2 is released"
date:     2022-10-19
category: releases
tags:     []
---
[Mesa 22.2.2](https://docs.mesa3d.org/relnotes/22.2.2.html) is released.
This is a bug fix release.
