---
title:    "Mesa 7.4.3 is released"
date:     2009-06-19
category: releases
tags:     []
---
[Mesa 7.4.3](https://docs.mesa3d.org/relnotes/7.4.3.html) is released. This is a stable release
fixing bugs since the 7.4.2 release.
