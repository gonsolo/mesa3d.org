---
title:    "Mesa 24.0.5 is released"
date:     2024-04-10
category: releases
tags:     []
---
[Mesa 24.0.5](https://docs.mesa3d.org/relnotes/24.0.5.html) is released.
This is a bug fix release.
