---
title:    "Mesa 19.3.2 is released"
date:     2020-01-09
category: releases
tags:     []
---
[Mesa 19.3.2](https://docs.mesa3d.org/relnotes/19.3.2.html) is released. This is a bug fix
release.
