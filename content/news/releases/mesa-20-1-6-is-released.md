---
title:    "Mesa 20.1.6 is released"
date:     2020-08-19
category: releases
tags:     []
---
[Mesa 20.1.6](https://docs.mesa3d.org/relnotes/20.1.6.html) is released. This is a bug fix release.
