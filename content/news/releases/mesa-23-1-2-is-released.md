---
title:    "Mesa 23.1.2 is released"
date:     2023-06-09
category: releases
tags:     []
---
[Mesa 23.1.2](https://docs.mesa3d.org/relnotes/23.1.2.html) is released.
This is a bug fix release.
