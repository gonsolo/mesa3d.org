---
title:    "April 4, 2007"
date:     2007-04-04 00:00:00
category: misc
tags:     []
---
Thomas Hellström of Tungsten Graphics has written a whitepaper
describing the new DRI memory management system.
